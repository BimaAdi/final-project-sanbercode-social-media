<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('posts/user/', 'postController@user');
    Route::post('post/like/', 'postController@like');
    Route::delete('post/unlike/{id}', 'postController@unlike');
    Route::get('post/destroy/{id}', 'postController@destroy');
    Route::post('post/{id}/comment', 'postController@comment');
    Route::post('profile/newpost', 'profileController@store2');
    Route::put('comment/{id}/toggleLike', 'CommentController@toggleLike');
    Route::resource('profile', 'profileController');
    Route::resource('post', 'postController');
    Route::resource('follower', 'FollowerController');
    //Route::delete('/follower/{id}','FollowerController@destroy');
    //Route::get('follower/destroy/{id}', 'FollowerController@destroy');
    Route::get('/awal', function () {
        return view('layouts.follower.follower');
    });
});




