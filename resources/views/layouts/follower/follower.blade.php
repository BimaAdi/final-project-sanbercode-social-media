@extends('layouts.master')

@section('content')

<div class="container">
    <br>
    <h5 style="font-weight: bold">Sudah difollow</h5>
    <table class="table">
        <thead>
          <tr>
            {{-- <th scope="col">#</th> --}}
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            @forelse($unfollows as $key => $unfollow)
            {{-- <td> {{ $key + 1}} </td> --}}
            <td> {{ $unfollow->name }} </td>
            <td>{{ $unfollow->email }}</td>
            <td>
                <form action="{{ route('follower.destroy', ['follower' => $unfollow->id]) }}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" value="Unfollow" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
          @empty
            <tr>
                <td colspan="4" align="center">Data kosong</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    
    <br>
    
    <h5 style="font-weight: bold">Belum difollow</h5>
    <table class="table">
        <thead>
          <tr>
            {{-- <th scope="col">#</th> --}}
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            @forelse($follows as $key2 => $follow)
            {{-- <td> {{ $key2 + 1}} </td> --}}
            <td> {{ $follow->name }} </td>
            <td>{{ $follow->email }}</td>
            <td>
              <form role="form" action="/follower" method="POST">
                @csrf
                <input type="hidden" class="form-control" id="userfollow_id" name="userfollow_id" value={{ $follow->id }}>
                <input type="submit" value="Follow" class="btn btn-info btn-sm">
              </form>
            </td>
          </tr>
          @empty
          <tr>
              <td colspan="4" align="center">Data kosong</td>
          </tr>
        @endforelse
        </tbody>
      </table>
</div>



@endsection