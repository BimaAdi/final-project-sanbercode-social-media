<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{asset('template/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('template/css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('template/css/me.css')}}" rel="stylesheet">
    <title>SanberCode Social Media</title>
</head>
<body style="background-color: #fafafa;">
    <nav class="navbar navbar-expand-lg navbar-light  shadow-sm  ">
        <div class="container">
            <a class="navbar-brand" href="{{ route('post.index') }}">Logo</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarNav">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active float-right">
                  <a class="nav-link" href="{{ route('post.index') }}">Beranda<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mr-4">
                  <a class="nav-link" href="{{route('follower.index')}}">Teman</a>
                </li>
               
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small" style="font-size: 15px;">{{Auth::user()->name}}</span>
                        @if (Auth::user()->profile != null)
                          <img class="img-profile rounded-circle" width="28" height="28"
                              src="{{'images/' . Auth::user()->profile->img}}">
                        @else
                          <img class="img-profile rounded-circle" width="28" height="28"
                          src="/images/blank-profile-picture.png">
                        @endif
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                        aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{route('profile.index')}}">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profil
                        </a>
                       
                  
                          
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                              
                         @csrf
                            <button class="btn btn-primary "  style="margin-left:12px;background: white; color:black ; border:none;">  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Keluar</button>
                        </form>
                        
                    </div>
                </li>
              </ul>
            </div>
        </div>
       
      </nav>

      @yield('content')

      <script src="{{asset('template/vendor/jquery/jquery.min.js')}}"></script>
      <script src="{{asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  
      <!-- Core plugin JavaScript-->
      <script src="{{asset('template/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  
      <!-- Custom scripts for all pages-->
      <script src="{{asset('template/js/sb-admin-2.min.js')}}"></script>

      @stack('uploads')


</body>
</html>