@extends('layouts.master')


@section('content')
    
<div class="container mt-3">
  <!-- success message -->

  <div class="container mt-3">
    <div class="row justify-content-md-center">
      <div class="col-6">
        <!-- Show error message -->
        @if($errors)
          @foreach ($errors->all() as $error)
              <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
              </div>
          @endforeach
        @endif
        <div class="card cards shadow">
          <div class="card-body">
              <div class="row">
                  <div class="col-2">
                    @if (Auth::user()->profile != null)
                      <img class="img-profile rounded-circle" width="28" height="28"
                          src="{{'images/' . Auth::user()->profile->img}}">
                    @else
                      <img class="img-profile rounded-circle" width="28" height="28"
                      src="/images/blank-profile-picture.png">
                    @endif
                  </div>
                  <div class="col-10">
                      <button type="button" class="btns btn-sm  btnnewpost " data-toggle="modal" data-target=".bd-example-modal-lg">Apa yang anda pikirkan sekarang?</button>
                  </div>
              </div>
              
            
              <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-body">
                          <form action="{{route('post.store')}}" method="POST" enctype="multipart/form-data">
                              @csrf
                              <div class="row">
                                  <div class="col-6">
                                  
                                          <div class="form-group">
                                  
                                              <div class="preview_holder">
                                                  <div id="preview2">
                                                  <img src="" id="preview_img" class="preview_img" height="200" width="200" />
                                                  <span id="preview_text" class="preview_text"></span>
                                                  </div>
                                              </div>
                                              <!-- Input Markup -->
                                              <input type="file" id="default-file" name="img" hidden="hidden" />
                                      
                                              <!-- Button Markup -->
                                              <button id="custom-btn" type="button"  class="btns mt-2 mx-auto btn-sm" >
                                              Upload gambar
                                              </button>
                                              <br>
                                      
                                              <!-- Choose File TEXT Markup -->
                                              <span id="custom-space" style="color: black;"> <strong> No</strong> File, Selected!😭</span>
                                          
                                          </div>
                                    
                                  
                                  </div>
                                  <div class="col-6">
                                      <div class="form-group ">
                                          <input type="text" class="form-control form-control-user form-control @error('name') is-invalid @enderror" 
                                          name="caption" value="{{ old('caption') }}" required autocomplete="caption" placeholder="caption" autofocus
                                              >
                                      </div>
                                      <div class="form-group ">
                                          <input type="text" class="form-control form-control-user form-control @error('name') is-invalid @enderror" 
                                          name="quote" value="{{ old('quote') }}" required autocomplete="quote" placeholder="quote" autofocus
                                              >
                                      </div>
                                      <div class="form-group">
                                          <textarea class="form-control  form-control-user"
                                              id="exampleInputPassword" name="post" rows="9" ></textarea>
                                      </div>
                                      <button type="submit" class="btn-user btns mx-auto ">Posting</button>
                                  </div>
                              </div>
                          </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>

        @forelse ($post as $item )
        <div class="card cards shadow mt-2 mb-3">
          <div class="card-body">
            <div class="row">
                <div class="col-1">
                  @if ($item->user->profile != null)
                    <img src="{{asset('images/'. $item->user->profile->img)}}" height="40" width="40" class="rounded-circle" alt="">
                  @else
                    <img src="{{asset('images/blank-profile-picture.png')}}" height="40" width="40" class="rounded-circle" alt="">
                  @endif
                </div>
                <div class="col-8  ml-3">
                  <?php  $namauser = Auth::user()->where('id', $item->user_id)->first(); ?>
                <a href="#"  class="link-dark">{{$namauser->name}}</a>
                    <br>
                <p  style="font-size: 10px ;">{{$item->created_at}}
                
                </div>
                
            </div>
          </div>

          <img class="card-img-top" src="{{asset('images/' . $item->img)}}"  height="300" width="300" style="background-size: cover; margin-top: -20px;" alt="Card image cap">
            <div class="card-body">
            <h5 class="card-title">{{$item->caption}}</h5>
            <p class="card-text">{{$item->post}}</p>
            <p class="card-text">{{$item->quote}}</p><hr>
            <div class="row mt-2">
             
               <?php $check= DB::table('likes')->where('user_id', Auth::id())->where('post_id',$item->id)->get();?>
               <?php $count= DB::table('likes')->where('post_id',$item->id)->count();?>
                
              @if ($check->isNotEmpty())
              <?php $id =$check[0]->id?>
              <div class="col-6 "> 
                <form action="/post/unlike/{{$id}}" method="POST">
                  @csrf
                  @method('delete')
                
                  <button class="btn btnlike " style="margin:auto;" type="submit"><i class="fa fa-thumbs-up fa-lg mb-2" style="color:blue;"></i>&nbsp;{{$count}} Disukai</button>
                </form>
              </div>
              @else
             
              <div class="col-6 "> 
                <form action="/post/like" method="POST">
                  @csrf
                 
                  <input type="hidden" name="idpost" value="{{$item->id}}">
                  <button class="btn btnlike " type="submit" style="margin:auto;"><i class="fa fa-thumbs-up fa-lg mb-2" style="color:grey;"></i>&nbsp;{{$count}} Suka</button>
                </form>
              </div>
              @endif
              <div class="col-6 ">
                  <button class="btn btnlike " style="margin:auto;display:flex;"><i class="fa fa-comment fa-lg mt-1" ></i>&nbsp; {{ $item->comments->count()}} Komentar</button>
              </div>

            </div>
            <hr>
              <!-- list komentar -->
            @foreach ($item->comments as $comment)
            <div class="row mt-1">
              <div class="col-1">
                @if ($comment->user->profile != null)
                  <img src="{{asset('images/'. $comment->user->profile->img)}}" height="40" width="40" class="rounded-circle" alt="">
                @else
                  <img src="{{asset('images/blank-profile-picture.png')}}" height="40" width="40" class="rounded-circle" alt="">
                @endif
              </div>
              <div class="col-9 ">
                <div class="ml-2" style="background-color: #eeeeee; border-radius:20px;margin-top:-5px;">
                  <h6 style="color:black;font-size:13px;font-weight:800;" class=" pt-2 pl-2 pr-2 ml-2 mt-1">{{ $comment->user->name}}</h6>
                  <p style="font-size: 12px;margin-top:-8px;" class="ml-2  pb-2 pl-2 pr-2">{{ $comment->komen }}</p>
                </div>
                
              </div>

              <div class="col-2 mt-2">
                <form action={{ url('comment/'.$comment->id.'/toggleLike') }} method="POST">
                  @csrf
                  @method('PUT')
                  <button class="btn btnlike btn-sm" style="margin-left: -20px;" type="submit" >
                    @if ($comment->likeComments()->where('user_id', Auth::id())->count() > 0)
                      <i class="fa fa-thumbs-up fa-sm " style="color:blue;"></i>  
                    @else
                      <i class="fa fa-thumbs-up fa-sm " style="color:grey;"></i>
                    @endif
                    &nbsp;{{$comment->likeComments()->count()}} Like
                  </button>
                </form>
              </div>
            </div>
          @endforeach
          <!-- end komentar -->

                      
            <!-- komentar -->
            <!-- add komentar -->
              <hr>
              <div class="row">
                <div class="col-1 mt-1 ">
                 
                    <img src="{{asset('images/'. Auth::user()->profile->img)}}" height="30" width="30" class="rounded-circle" alt="">
               
                </div>
                <div class="col-11">
                  <form action="{{ url('post/'.$item->id.'/comment') }}" method="POST" class="form-inline d-flex justify-content-between mb-2">
                    @csrf
                    <div class="row ml-2">
                      <div class="">
                        <div class="form-group">
                          <input type="text" class="form-control ml-1" style="background-color: #eeeeee; border-radius:20px;border:none;" name='komen' placeholder="Tulis komentar anda disini" />
                        </div>
                      </div>
                   
                    </div>
                    <div class="row">
                      <button class="btn btn-sm btns" type="submit">send</button>
                    </div>
                    
                  </form>
                </div>
               
              </div>

            </div>
        </div>


      

    @empty
        <div class="container mx-auto">
          <img src="{{asset('images/no-data-concept-illustration_114360-616.jpg')}}" style="margin:auto;display:flex;" width="250" height="250" alt="">
          <p class="text-center" style="color:black;font-weight:700;">Maaf Belum ada post</p>
        </div>
    @endforelse

    
          
      </div>
    </div>
  </div>
  @if (session('success'))
  <div class="toast" data-autohide="false">
    <div class="toast-header">
      <strong class="mr-auto text-primary">Toast Header</strong>
      <small class="text-muted">5 mins ago</small>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
    </div>
    <div class="toast-body">
      Some text inside the toast body
    </div>
  </div>
</div>
  @endif
  
@endsection


@push('uploads')
<script>
  $('.toast').toast('show')
    // Grabbing Elements and Storing in Variables
const defaultFile = document.getElementById("default-file");
const customBtn = document.getElementById("custom-btn");
const customSpace = document.getElementById("custom-space");
customBtn.addEventListener("click", function () {
  defaultFile.click();
});

// File Upload
defaultFile.addEventListener("change", function () {
  //  Format Selected File Text
  if (defaultFile.value) {
    customSpace.innerHTML =
      defaultFile.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1] + "🔥";
  } else {
    customSpace.innerHTML = "No File, Selected!😭";
  }

  // Image Preview
  const files = defaultFile.files[0]; //files[0] - For getting first file
  //   console.log(files);

  if (files) {
    // Showing Image and Hiding "Image Preview" Text
    preview_img.style.display = "block";
    preview_text.style.display = "none";
    //Read File
    const fileReader = new FileReader();

    fileReader.addEventListener("load", function () {
      // convert image to base64 encoded string
      preview_img.setAttribute("src", this.result);
      console.log(this.result);
    });
    fileReader.readAsDataURL(files);
  }
});


function handlehapus(id){
  var link = document.getElementById('hapuslink')
  link.href="{{URL::to('post/destroy')}}/" + id
  $('#hapus').modal('show');
}
</script>
@endpush