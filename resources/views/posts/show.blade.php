@extends('layouts.master')


@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="card">
                <!-- User Section-->
                <div class="card-body">
                <div class="row">
                    <div class="col-1">
                    <img src="{{asset('template/img/login.jpg')}}" height="40" class="rounded-circle" alt="">
                    </div>
                    <div class="col-8  ml-3">
                        <a href=""  class="link-dark">{{$post->user->name}}</a>
                        <br>
                        
                    </div>
                    <div class="col-3 "></div>
                </div>
                    
                </div>

                <!-- Post Section-->
                <img class="card-img-top" src="{{asset('template/img/login.jpg')}}"  height="400" style="background-size: cover; margin-top: -20px;" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ url('post/'.$post->id) }}">{{ $post->caption }}</a></h5>
                    <p class="card-text">{{ $post->quote }}</p>
                    <p class="card-text">{{ $post->post }}</p>
                    <div class="row mt-2">
                    <div class="col-4 ">
                        <p class="text-center">14 like</p> 
                    </div>
                    <div class="col-4">
                        <p class="text-center">14 like</p> 
                    </div>
                    <div class="col-4">
                        <p class="text-center">14 like</p> 
                    </div>
                    </div>
                </div>
                
            </div><!-- end card -->
        </div>
        <div class="col-3"></div>
</div>
@endsection
