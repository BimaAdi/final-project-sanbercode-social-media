@extends('layouts.master')

@section('content')
    <div class="container">
      <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-2  mx-auto  ">
                            <img src="{{asset('images/'. $post->img)}}" class="ml-2" style="width: 150px;height:150px;background-size:cover;margin-top:200px;" alt="">
                        </div>
                        <div class="col-lg-10">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Ubah data post kamu</h1>
                                </div>
                                <form action="{{route('post.update',$post->id)}}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  @method('put')
                                  <div class="row">
                                      <div class="col-6">
                                      
                                              <div class="form-group">
                                      
                                                  <div class="preview_holder">
                                                      <div id="preview2">
                                                      <img src="" id="preview_img" class="preview_img" height="200" width="200" />
                                                      <span id="preview_text" class="preview_text"></span>
                                                      </div>
                                                  </div>
                                                  <!-- Input Markup -->
                                                  <input type="file" id="default-file" name="img" hidden="hidden" />
                                          
                                                  <!-- Button Markup -->
                                                  <button id="custom-btn" type="button"  class="btns mt-2 mx-auto btn-sm" >
                                                  Upload gambar
                                                  </button>
                                                  <br>
                                          
                                                  <!-- Choose File TEXT Markup -->
                                                  <span id="custom-space" style="color: black;"> <strong> No</strong> File, Selected!😭</span>
                                              
                                              </div>
                                        
                                      
                                      </div>
                                      <div class="col-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control form-control-user form-control @error('caption') is-invalid @enderror" 
                                              name="caption" value="{{ old('caption')  }}{{$post->caption}}" required autocomplete="caption" placeholder="caption" autofocus
                                                  >
                                          </div>
                                          <div class="form-group ">
                                              <input type="text" class="form-control form-control-user form-control @error('quote') is-invalid @enderror" 
                                              name="quote" value="{{ old('quote') }}{{$post->quote}}" required autocomplete="quote" placeholder="quote" autofocus
                                                  >
                                          </div>
                                          <div class="form-group">
                                              <textarea class="form-control  form-control-user"
                                                  id="exampleInputPassword" name="post" rows="9" >{{$post->post}}</textarea>
                                          </div>
                                          <div class="row">
                                            <div class="col-6">
                                              <a href="{{route('profile.index')}}" class=" btn btn-secondary btns mx-auto ">Kembali</a>
                                            </div>
                                            <div class="col-6">
                                              <button type="submit" class="btn btn-primary btns mx-auto ">Update post</button>
                                            </div>
                                          </div>
                                          
                                         
                                      </div>
                                  </div>
                              </form>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    </div>
@endsection


@push('uploads')
  <script>

    // Grabbing Elements and Storing in Variables
    const defaultFile = document.getElementById("default-file");
const customBtn = document.getElementById("custom-btn");
const customSpace = document.getElementById("custom-space");
customBtn.addEventListener("click", function () {
  defaultFile.click();
});

// File Upload
defaultFile.addEventListener("change", function () {
  //  Format Selected File Text
  if (defaultFile.value) {
    customSpace.innerHTML =
      defaultFile.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1] + "🔥";
  } else {
    customSpace.innerHTML = "No File, Selected!😭";
  }

  // Image Preview
  const files = defaultFile.files[0]; //files[0] - For getting first file
  //   console.log(files);

  if (files) {
    // Showing Image and Hiding "Image Preview" Text
    preview_img.style.display = "block";
    preview_text.style.display = "none";
    //Read File
    const fileReader = new FileReader();

    fileReader.addEventListener("load", function () {
      // convert image to base64 encoded string
      preview_img.setAttribute("src", this.result);
      console.log(this.result);
    });
    fileReader.readAsDataURL(files);
  }
});
    </script>
@endpush