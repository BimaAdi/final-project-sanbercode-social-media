@extends('layouts.master')


@section('content')
    
<div class="container mt-3">
  <!-- success message -->
  @if (session('success'))
    <div id="exampleInputEmail1-error" class="alert alert-success alert-dismissible fade show" role="alert">
      {{ session('success') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif

  <div class="row">
    <div class="col-3"></div>
    <div class="col-6">
      <h1>Your Posts</h1>
      <a href="/post/create" class="btn btn-primary mb-1">Buat Post</a>

      @forelse ($posts as $post)
        <div class="card">
          <!-- User Section-->
          <div class="card-body">
            <div class="row">
              <div class="col-1">
                <img src="{{asset('template/img/login.jpg')}}" height="40" class="rounded-circle" alt="">
              </div>
              <div class="col-8  ml-3">
                  <a href=""  class="link-dark">{{$post->user->name}}</a>
                  <br>
                  
              </div>
              <div class="col-3 "></div>
            </div>
              
          </div>

          <!-- Post Section-->
          <img class="card-img-top" src="{{asset('template/img/login.jpg')}}"  height="400" style="background-size: cover; margin-top: -20px;" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title"><a href="{{ url('post/'.$post->id) }}">{{ $post->caption }}</a></h5>
              <p class="card-text">{{ $post->quote }}</p>
              <p class="card-text">{{ $post->post }}</p>
              <div class="row mt-2">
                <div class="col-6 ">
                    <p class="text-center"><a href="{{ url('post/'.$post->id.'/edit/') }}">Edit</a></p> 
                </div>
                <div class="col-6">
                  <form action="{{ url('post/'.$post->id.'/') }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="text-center" style="background: none;color: #224abe;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit;" type="submit">
                      Delete
                    </button> 
                  </form>
                </div>
              </div>
            </div>
          
        </div><!-- end card -->
      @empty
          <h3>Post tidak ditemukan</h3>
      @endforelse

      </div>
      <div class="col-4">

      </div>
    </div>
</div>
@endsection