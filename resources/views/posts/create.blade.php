@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="card mt-3">

            <!-- Form create Post -->
            <div class="card-body">
                <form method="POST" action="{{ url('post') }}">
                    @csrf
                    {{-- nanti diubah jadi upload file --}}
                    {{-- <div class="form-group row">
                      <label for="staticEmail" class="col-sm-2 col-form-label">Image</label>
                      <div class="col-sm-10">
                        <input type="file" class="form-control-file" name="img">
                      </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="img" class="col-sm-2 col-form-label">Gambar</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="img" value="{{ old('img') }}">
                        @error('img')
                            <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror
                        </div>
                      </div>
                    <div class="form-group row">
                      <label for="caption" class="col-sm-2 col-form-label">Caption</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="caption" value="{{ old('caption') }}">
                        @error('caption')
                            <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="quote" class="col-sm-2 col-form-label">Quote</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control" name="quote" value="{{ old('quote') }}">
                        @error('quote')
                            <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Teks</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" aria-label="With textarea" name="post">{{ old('post') }}</textarea>
                            @error('post')
                                <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ $message }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @enderror
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
@endsection