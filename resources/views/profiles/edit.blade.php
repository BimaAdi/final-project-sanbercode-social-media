@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block ">
                            <img src="{{asset('images/'. $data->img)}}" style="width: 100%;height:100%;background-size:cover;" alt="">
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Ubah Data Profile Kamu</h1>
                                </div>
                                <form class="user" action="{{route('profile.update',$data->id)}}" method="POST" enctype="multipart/form-data">
                                    
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                
                                        <div class="preview_holder">
                                            <div id="preview">
                                              <img src="" id="preview_img" class="preview_img rounded-circle" height="90" width="90" />
                                              <span id="preview_text" class="preview_text"></span>
                                            </div>
                                          </div>
                                        <!-- Input Markup -->
                                        <input type="file" id="default-file" name="img" hidden="hidden" />
                                  
                                        <!-- Button Markup -->
                                        <button id="custom-btn" type="button"  class="btns mt-2 mx-auto btn-sm" >
                                         upload gambar
                                        </button>
                                        <br>
                                  
                                        <!-- Choose File TEXT Markup -->
                                        <span id="custom-space" style="color: black;"> <strong> No</strong> File, Selected!😭</span>
                                      
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control  form-control-user"
                                            id="exampleInputPassword" name="bio" placeholder="bio">{{$data->bio}}</textarea>
                                    </div>
                                    
                                  
                                      
                                    <button type="submit"  class="mt-3 btn btn-primary btn-user btn-block">
                                        Update profile
                                    </button>
                                    <a href="{{route('profile.index')}}" type="submit"  class="mt-3 btn btn-secondary btn-user btn-block">
                                        kembali
                                    </a>
                                
                            
                                   
                                </form>
                
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
@endsection



@push('uploads')
<script>
    // Grabbing Elements and Storing in Variables
const defaultFile = document.getElementById("default-file");
const customBtn = document.getElementById("custom-btn");
const customSpace = document.getElementById("custom-space");
customBtn.addEventListener("click", function () {
  defaultFile.click();
});

// File Upload
defaultFile.addEventListener("change", function () {
  //  Format Selected File Text
  if (defaultFile.value) {
    customSpace.innerHTML =
      defaultFile.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1] + "🔥";
  } else {
    customSpace.innerHTML = "No File, Selected!😭";
  }

  // Image Preview
  const files = defaultFile.files[0]; //files[0] - For getting first file
  //   console.log(files);

  if (files) {
    // Showing Image and Hiding "Image Preview" Text
    preview_img.style.display = "block";
    preview_text.style.display = "none";
    //Read File
    const fileReader = new FileReader();

    fileReader.addEventListener("load", function () {
      // convert image to base64 encoded string
      preview_img.setAttribute("src", this.result);
      console.log(this.result);
    });
    fileReader.readAsDataURL(files);
  }
});
</script>
@endpush