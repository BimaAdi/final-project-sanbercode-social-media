@extends('layouts.master')

@section('content')
    <div class="container mt-3">
        <div class="row justify-content-md-center">
            <div class="col-4">
                <div class="card cards shadow">
                    <div class="card-body">
                        @if (Auth::user()->profile != null)
                          <img class="img-profile rounded-circle" width="28" height="28"
                            src="{{'images/' . Auth::user()->profile->img}}">
                        @else
                          <img class="img-profile rounded-circle" width="28" height="28"
                          src="/images/blank-profile-picture.png">
                        @endif
                        <p class="text-dark text-center mt-2" style="font-weight:700;font-size:20px;">{{Auth::user()->name}}
                        <p class="text-center" style="font-size: 12px; margin-top:-10px;">{{Auth::user()->email}}</p>

                        <div class="row">
                            <div class="col-4 text-center">
                                <p>{{$countpost}}</p>
                                <p>Post</p>
                            </div>
                            <div class="col-4 text-center">
                                <p>{{$countfollow}}</p>
                                <p>Following</p>
                            </div>
                            <div class="col-4 text-center">
                                <p>{{$countfollower}}</p>
                                <p>Follower</p>
                            </div>
                        </div>
                        <div class="row mt-3 mx-auto">
                          <p class="text-center">
                            @if (Auth::user()->profile != null)
                              {{Auth::user()->profile->bio}}
                            @endif
                          </p>
                      </div>
                      <div class="row mt-4 mx-auto">
                    
                        <a href="{{route('profile.edit', Auth::user()->id)}}" class="btns btn btn-sm mx-auto"> Ubah profile</a>
                      </div>
                    </div>
                   
                </div>
            </div>
            <div class="col-5 mb-3">
                <!-- Show error message -->
                  @if($errors)
                    @foreach ($errors->all() as $error)
                        <div id="exampleInputEmail1-error" class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{ $error }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    @endforeach
                  @endif
                  @if (session('hapus'))
                  <div class="alert alert-danger">
                      {{session('hapus')}}
                  </div>
                  @endif
                  @if (session('edit'))
                  <div class="alert alert-success">
                      {{session('edit')}}
                  </div>
                  @endif
                <div class="card cards shadow">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                              @if (Auth::user()->profile != null)
                                <img class="img-profile rounded-circle" width="28" height="28"
                                  src="{{'images/' . Auth::user()->profile->img}}">
                              @else
                                <img class="img-profile rounded-circle" width="28" height="28"
                                src="/images/blank-profile-picture.png">
                              @endif
                            </div>
                            <div class="col-10">
                                <button type="button" class="btns btn-sm  btnnewpost " data-toggle="modal" data-target=".bd-example-modal-lg">Apa yang anda pikirkan sekarang?</button>
                            </div>
                        </div>
                        
                      
                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                               <div class="modal-body">
                                <form action="/profile/newpost" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-6">
                                        
                                                <div class="form-group">
                                        
                                                    <div class="preview_holder">
                                                        <div id="preview2">
                                                        <img src="" id="preview_img" class="preview_img" height="200" width="200" />
                                                        <span id="preview_text" class="preview_text"></span>
                                                        </div>
                                                    </div>
                                                    <!-- Input Markup -->
                                                    <input type="file" id="default-file" name="img" hidden="hidden" />
                                            
                                                    <!-- Button Markup -->
                                                    <button id="custom-btn" type="button"  class="btns mt-2 mx-auto btn-sm" >
                                                    Upload gambar
                                                    </button>
                                                    <br>
                                            
                                                    <!-- Choose File TEXT Markup -->
                                                    <span id="custom-space" style="color: black;"> <strong> No</strong> File, Selected!😭</span>
                                                
                                                </div>
                                          
                                        
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group ">
                                                <input type="text" class="form-control form-control-user form-control @error('name') is-invalid @enderror" 
                                                name="caption" value="{{ old('caption') }}" required autocomplete="caption" placeholder="caption" autofocus
                                                    >
                                            </div>
                                            <div class="form-group ">
                                                <input type="text" class="form-control form-control-user form-control @error('name') is-invalid @enderror" 
                                                name="quote" value="{{ old('quote') }}" required autocomplete="quote" placeholder="quote" autofocus
                                                    >
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control  form-control-user"
                                                    id="exampleInputPassword" name="post" rows="9" ></textarea>
                                            </div>
                                            <button type="submit" class="btn-user btns mx-auto ">Posting</button>
                                        </div>
                                    </div>
                                </form>
                               </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                </div>

                @forelse ($post as $item )
                    <div class="card cards shadow mt-2">
                      <div class="card-body">
                        <div class="row">
                            <div class="col-1">
                            @if (Auth::user()->profile != null)
                              <img src="{{asset('images/'. Auth::user()->profile->img)}}" height="40" width="40" class="rounded-circle" alt="">  
                            @else
                              <img src="{{asset('images/blank-profile-picture.png')}}" height="40" width="40" class="rounded-circle" alt="">
                            @endif
                            </div>
                            <div class="col-8  ml-3">
                            <a href="#"  class="link-dark">{{Auth::user()->name}}</a>
                                <br>
                            <p  style="font-size: 10px ;">{{$item->created_at}}
                            
                            </div>
                            <div class="col-2 ml-auto">
                              <button class="btn btnlike"  id="dropdownMenuButton" data-toggle="dropdown"><i class="fa fa-ellipsis-h  fa-lg"></i></button>
                              <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('post.edit',$item->id)}}" >Edit Postingan</a>
                                <button class="dropdown-item" onclick="handlehapus({{$item->id}})">Hapus Postingan</button>
                             


                                </div>
                            </div>
                        </div>
                      </div>

                      <img class="card-img-top" src="{{asset('images/' . $item->img)}}"  height="300" width="300" style="background-size: cover; margin-top: -20px;" alt="Card image cap">
                        <div class="card-body">
                        <h5 class="card-title">{{$item->caption}}</h5>
                        <p class="card-text">{{$item->post}}</p>
                        <p class="card-text">{{$item->quote}}</p><hr>
                        <div class="row mt-2">
                           
                            <div class="col-6 "> 
                              <button class="btn btnlike "><i class="fa fa-thumbs-up fa-lg mb-2"></i>&nbsp;14 Like</button>
                            </div>
                            <div class="col-6 ">
                              <button class="btn btnlike "><i class="fa fa-comment fa-lg mt-1" ></i>&nbsp; 14 Comment</button>
                            </div>
                        </div>
                        </div>
                    </div>
                @empty
                    <div class="container mx-auto">
                      <img src="{{asset('images/no-data-concept-illustration_114360-616.jpg')}}" style="margin:auto;display:flex;" width="250" height="250" alt="">
                      <p class="text-center" style="color:black;font-weight:700;">Maaf belum ada post</p>
                    </div>
                @endforelse

                <div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                     
                      <div class="modal-body">
                        Apakah kamu yakin menghapus postingan ini
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">tutup</button>
                        <a type="button" id="hapuslink" class="btn btn-danger">yakin hapus</a>
                      </div>
                    </div>
                  </div>
                </div>
          
               
               
                
            </div>
        </div>
    </div>
@endsection

@push('uploads')
<script>
    // Grabbing Elements and Storing in Variables
const defaultFile = document.getElementById("default-file");
const customBtn = document.getElementById("custom-btn");
const customSpace = document.getElementById("custom-space");
customBtn.addEventListener("click", function () {
  defaultFile.click();
});

// File Upload
defaultFile.addEventListener("change", function () {
  //  Format Selected File Text
  if (defaultFile.value) {
    customSpace.innerHTML =
      defaultFile.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1] + "🔥";
  } else {
    customSpace.innerHTML = "No File, Selected!😭";
  }

  // Image Preview
  const files = defaultFile.files[0]; //files[0] - For getting first file
  //   console.log(files);

  if (files) {
    // Showing Image and Hiding "Image Preview" Text
    preview_img.style.display = "block";
    preview_text.style.display = "none";
    //Read File
    const fileReader = new FileReader();

    fileReader.addEventListener("load", function () {
      // convert image to base64 encoded string
      preview_img.setAttribute("src", this.result);
      console.log(this.result);
    });
    fileReader.readAsDataURL(files);
  }
});


function handlehapus(id){
  var link = document.getElementById('hapuslink')
  link.href="{{URL::to('post/destroy')}}/" + id
  $('#hapus').modal('show');
}
</script>
@endpush