<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    protected $guarded=[];
  
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User','likes');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','post_id');
    }
}
