<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'likecomments';

    public $timestamps = false;

    protected $fillable = [
        'comment_id', 'user_id'
    ];

    public function comment()
    {
        return $this->belongsTo('App/Comment');
    }

    public function user()
    {
        return $this->belongsTo('App/User');
    }
}
