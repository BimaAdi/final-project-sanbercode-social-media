<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /*
    *
    * Toggle like, 
    * jika comment sudah di like oleh user maka menjadi unlike
    * jika commnent belum di like oleh user maka menjadi dislike
    */
    public function toggleLike($id) 
    {
        $comment = Comment::find($id);
        $isCommentlikedByUser = $comment->likeComments()->where('user_id', Auth::id())->count() > 0;

        if ($isCommentlikedByUser == True) {
            $comment->likeComments()->where('user_id', Auth::id())->delete();
        } else {
            $comment->likeComments()->create([
                'user_id' => Auth::id()
            ]);
        }

        return redirect('/post');
    }
}
