<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use DB;
use Auth;
use App\User;

class FollowerController extends Controller
{
    public function index()
    {
        
        //$follows= Follow::where('user_id' , Auth::id())->get();
        $unfollows = Follow::join('users','follows.userfollow_id', '=', 'users.id')->where('follows.user_id', Auth::id())->get(['users.name', 'users.email', 'users.id']);
        $count = Follow::select('userfollow_id')->where('user_id', '=', Auth::id())->get();
        //$follows = User::select('users.name', 'users.email', 'users.id')->whereNotIn('id', Follow::select('userfollow_id')->where('users.id', Auth::id()))->where('users.id', '<>', Auth::id())->get();
        $follows = User::select('users.name', 'users.email', 'users.id')->whereNotIn('users.id', ($count))->where('users.id', '<>', Auth::id())->get();
        return view('layouts.follower.follower', compact('unfollows','follows'));
    }

    public function store(Request $request){

        //dd($request);
        $follow = new Follow;
        $follow->status_follow = null;
        $follow->follow = null;
        $follow->userfollow_id = $request['userfollow_id'];
        $follow->user_id =  Auth::id();
        $follow->save();

        //return view('layouts.follower');
        return redirect("/follower"); //sukses = session
    }

    public function destroy($id){
        
        $query = DB::table('follows')->where('userfollow_id', $id)->where('user_id', Auth::id())->delete();
        return redirect('/follower');
        // ->with('sukses', 'berhasil diunfollow');


    }

 
}
