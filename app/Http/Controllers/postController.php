<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use DB;
use App\Post;
use Auth;
use File;
use App\Like;



class postController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $post = Post::orderByDesc('id')->get();
       
        return view('home', compact('post'));
    }

  
    public function create()
    {
        return view('posts.create');
    }

    public function like(Request $request){
        $user = Post::findorfail($request->idpost);
       $user->users()->attach(Auth::id());


       return redirect('/post');
   
     
     
    }

    public function unlike($id){

       $unlike= Like::findorfail($id);
       $unlike->delete();
       return redirect('/post');
     
     
    }

  
    public function store(Request $request)
    { 
        $request->validate([
        "caption" => "required",
        "quote" => "required",
        "img" => "required|mimes:png,jpg,jpeg"
        ]); //validasi form
    

        $img = $request->img;
        $file_name = time() . "-" . $img->getClientOriginalName();
        Post::create([
            "post" => $request->post,
            'caption' => $request->caption,
            "quote" => $request->quote,
            'img' => $file_name,
            'user_id' => Auth::id()
        ]);

        $img->move('images', $file_name); 

         return redirect('/post')->with('success', 'Post Berhasil dihapus');
        
    }

    
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show', ['post' => $post]);
    }

  
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit', ['post' => $post]);
    }

   
    public function update(Request $request, $id)
    {
        $request->validate([
            "caption" => "required",
            "quote" => "required",
            "img" => "mimes:png,jpg,jpeg"
            ]); //validasi form
 

        $post = Post::findorfail($id);
        if ($request->has('img')) {
            $path= "images/";
            File::delete($path . $post->img);
            $img= $request->img;
            $newgambar = time() .'-' . $img->getClientOriginalName();
            $img->move('images' , $newgambar);
            $postdata = [
                "post" => $request->post,
                'caption' => $request->caption,
                "quote" => $request->quote,
                'img' => $newgambar,
                'user_id' => Auth::id()
            ];

        }else{
            $postdata =[
                "post" => $request->post,
                'caption' => $request->caption,
                "quote" => $request->quote,
                'user_id' => Auth::id()
            ];
        }
        
        $post->update($postdata);
        return redirect('/profile')->with('edit','post berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        
        $query= Post::findorfail($id);
        $hapuscomment= Comment::where('post_id',$id)->delete();
       
        $query->users()->sync([]);
  
        $query->delete();



        $path= "images/";
        File::delete($path, $query->img);

        return redirect('/profile')->with('hapus', 'Post Berhasil dihapus');
            
        // return redirect()
        // ->action('postController@user')
        // ->with('success', 'Post Berhasil dihapus');
    }

    /**
     * menampilkan post dari user login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function user(Request $request)
    {
        // cari semua posts milik user login
        $posts = Post::where('user_id', '=', Auth::id())->get();
        return view('posts.user', ['posts' => $posts]);
    }

    /**
     * menambahkan comment dari post
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function comment(Request $request, $id)
    {
        $request->validate([
            'komen' => 'required'
        ]);

        $post = Post::findOrFail($id);
        $post->comments()->create([
            'komen' => $request->input('komen'),
            'user_id' => Auth::id()
        ]);

        return redirect('/post')->with('success', 'komentar berhasil ditambahkan');;
    }
    
}
