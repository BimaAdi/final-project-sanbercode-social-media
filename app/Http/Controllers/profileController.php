<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Profile;
use App\Post;
use File;
use App\Follow;

class profileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
     
        $countpost= Post::where('user_id' , Auth::id())->count();
        $countfollow= Follow::where('user_id' , Auth::id())->count();
        $countfollower= Follow::where('userfollow_id' , Auth::id())->count();
        $post=Post::where('user_id' , Auth::id())->orderByDesc('id')->get();
        // dd($countpost);
        return view('profiles.index',compact('countpost','post','countfollow','countfollower'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profiles.profileAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "bio" => "required",
            "img" => "required|mimes:png,jpg,jpeg"
        ]); //validasi form
        

        $img = $request->img;
        $file_name = time() . "-" . $img->getClientOriginalName();
        Profile::create([
            'bio' => $request->bio,
            'img' => $file_name,
            'user_id' => Auth::id()
        ]);

        $img->move('images', $file_name); 

        return redirect('/post');
    }

    public function store2(Request $request)
    {
        $request->validate([
            "caption" => "required",
            "quote" => "required",
            "img" => "required|mimes:png,jpg,jpeg"
            ]); //validasi form
        
    
            $img = $request->img;
            $file_name = time() . "-" . $img->getClientOriginalName();
            Post::create([
                "post" => $request->post,
                'caption' => $request->caption,
                "quote" => $request->quote,
                'img' => $file_name,
                'user_id' => Auth::id()
            ]);
    
            $img->move('images', $file_name); 
    
             return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Profile::findorfail($id);
    
        return view('profiles.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "bio" => "required",
            "img" => "mimes:png,jpg,jpeg"
        ]); //validasi form

        $profile = Profile::findorfail($id);
        if ($request->has('img')) {
            $path= "images/";
            File::delete($path . $profile->img);
            $img= $request->img;
            $newgambar = time() .'-' . $img->getClientOriginalName();
            $img->move('images' , $newgambar);
            $profile_data = [
                'bio' => $request->bio,
                'img' => $newgambar
            ];

        }else{
            $profile_data =[
                'bio' => $request->bio
                
            ];
        }
        
        $profile->update($profile_data);
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
